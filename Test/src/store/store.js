import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
Vue.use(Vuex);

const store = new Vuex.Store({
    state: {
        AuthToken: ''
    },
    mutations: {
        ["SetToken"]: (state, payload) => {
            state.AuthToken = payload.AuthToken;
        },
        ["DeleteToken"]: (state, payload) => {
            state.AuthToken = ''
        }
    },
    actions: {
        ["CheckUser"]: (state, payload) => {
            axios.post("https://localhost:44365/api/v1/Users/authenticate", {
                UserName: payload.UserName,
                Password: payload.Password
            }).then((response) => {
                store.commit("SetToken", { AuthToken: response.data.authToken });
            }).catch(error => alert("로그인 오류"))
        },
        ["DeleteTokenAction"]: (state, payload) => {
            store.commit("DeleteToken")
        }
    }
})

export default store;